<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<?php include("head.php"); ?>
</head>
<body>
	<div id="container">
		<?php include("navigation.php"); ?>
		<div class="spacer"></div>
		<ul id="logo" class="logo">
			<li class="layer"><img src="img/logo/V1/layer1.png"></li>
		</ul>
			<ul id="language">
				<li>de</li>
				<li>//</li>
				<li>en</li>
			</ul>
			<div id="topImageWrapper">
				<div id="topImageProject">
					<div id="groupProjectImage">
						<img src="img/project/vertigo/vertigo-topImg-2.jpg">
						<img src="img/project/vertigo/vertigo-topImg-3.jpg">
						<img src="img/project/vertigo/vertigo-topImg-4.jpg">
						<img src="img/project/vertigo/vertigo-topImg-5.jpg">
						<img src="img/project/vertigo/vertigo-topImg-6.jpg">
						<img src="img/project/vertigo/vertigo-topImg-7.jpg">
						<img src="img/project/vertigo/vertigo-topImg-8.jpg">
						<img src="img/project/vertigo/vertigo-topImg-9.jpg">
					</div>
				</div>
			</div>
			<ul class="grid projectTitle" id="projectDescription">
				<h3>Vertigo</h3> 
				
				<div class="textSet deutschText">
					<p> 
						Das Projekt Vertigo soll, ohne dass der Besucher auf eine hohe 
						Erhebung steigen muss, das Gef&uuml;hl der H&ouml;henangst vermitteln.
						<br>
						<br>
						<strong>Warum?</strong>
						<br>
						Jeder kennt das Gef&uuml;hl von Angst, aber nicht alle das der H&ouml;henangst.
						Kann man durch die visuelle Simulation eines freien Falls, jedem
						Besucher ein H&ouml;henangstgef&uuml;hl erleben lassen? Vertigo soll dem
						Besucher den eigentlichen Stand vergessen lassen und in ihm ein
						Schwindelgef&uuml;hl erwecken.
						<br>
						<br>
						<strong>Resultat</strong>
						<br>
						Der Besucher sieht vor sich einen dunklen Raum, in dem mittig ein
						Podest mit Gel&auml;nder steht. Steigt der Besucher auf das Podest,
						erscheint das Video des freien Falls.
						<br>
						<br>
						<strong>Umsetzung</strong>
						<br>
						Das Team bestand aus 4 Personen. Das Konzept haben wir zusammen
						erarbeitet, ich habe f&uuml;r die Ausstellung die technische Umsetzung
						realisiert. Sobald der Besucher die Plattform betritt, wird er von der
						Microsoft Kinect erkannt und das Video wird abgespielt.
						Umgesetzt mit Processing.

					</p>
				</div>
				<video>		<!-- MP4 must be first for iPad! -->
					<source src="video/vertigo.mp4" type="video/mp4" />
					<source src="video/vertigo/vertigo_wondershare.webm" type="video/webm" />
				</video>
			</ul>
		</div>
		<script src="js/modernizr.custom.js"></script>
		<script src="js/onmediaquery.min.js"></script>
		<script src="js/myFunctions.js"></script>
		<script>
		// Picture element HTML5 shiv
		document.createElement( "picture" );
		</script>
		<script src="js/picturefill.js" async></script>
	
	</body>
</html>
