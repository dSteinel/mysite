$(window).scroll(function (event) {
    var y = $(this).scrollTop();
	if (y > 80) {
        $('#container').addClass('scroll');
    } 
    else{
	    $('#container').removeClass('scroll');
    }
});

function randomColor(){
	var myRandomColor = 'rgba(' + (Math.floor((256-199)*Math.random()) + 200) + ',' + (Math.floor((256-199)*Math.random()) + 200) + ',' + (Math.floor((256-199)*Math.random()) + 200) + ',.65)';
	return myRandomColor;
}
$( document ).ready(function() {
	randomColor();
	$(".grid li").mouseover(function(){
		$(this).find(".stack").addClass("active",1000);
	});
	$(".grid li").mouseout(function(){
		$(this).find(".stack").removeClass("active",1000);
	});

	$(".spacer").css("background-color", randomColor());
	
	$(".grid li, #navigation a").mouseover(function() {
		$(this).css("background-color", randomColor());
		$(this).find("figcaption").css("visibility", "visible").animate({ marginTop: "40px"}, 200 );
	}).mouseout(function() {
		$(this).css("background-color","#f6f6f6");
		$(this).find("figcaption").css("visibility", "hidden").animate({ marginTop: "40px"}, 50 );
	});
});
