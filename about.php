
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<?php include("head.php"); ?>
</head>
<body>
	<div id="container">
		<?php include("navigation.php"); ?>
		<div class="spacer"></div>
		<ul id="logo" class="logo">
			<li class="layer"><img src="img/logo/V1/layer1.png"></li>
		</ul>
		<!--
<ul id="language">
			<li>de</li>
			<li>//</li>
			<li>en</li>
		</ul>
-->
		<div id="aboutImageWrapper">
			<div id="topImageProject">
				<div id="meImage"></div>
			</div>
		</div>
		<ul class="grid projectTitle" id="projectDescription">
			<h3>Dimitri Steinel</h3> 
			
			<div class="textSet deutschText">
				<p> 
					Hey there,
					<br>
					My name is Dimitri Steinel a Berlin based interactiondesigner with some experience.
					I am currently a interactiondesign student at the btk-fh Berlin. I will finish my study in February '14 and I am really looking forward for some nice work. I have a passion for creating beautiful, intuitive, and highly interactive Installations. I have wide ranging experience in design from web and mobile design through to large scale interactive installations.
My portfolio is a mix of work created in the last few years.
If you are interested in working together I'm available for freelance, contract or direct client projects, contact details can be found below.
				</p>
				<br>
				<br>
				email: <a href="mailto:Dimitri.Steinel@gmail.com">Dimitri.Steinel@gmail.com</a>
			</div>
			
	</ul>
</div>
	<!-- Scripts -->
	<script src="js/modernizr.custom.js"></script>
	<script src="js/onmediaquery.min.js"></script>
	<script src="js/myFunctions.js"></script>
</body>
</html>
